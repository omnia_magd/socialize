import { Component, OnInit } from '@angular/core';
import  { AjaxService } from '../ajax.service';
import { Router } from '@angular/router';
import { AlertsService } from 'angular-alert-module';



@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: Array<object>;
  response: Array<object>;

  constructor(private ajax : AjaxService,private router : Router,private alerts: AlertsService) { 
      this.users = [];
    }

  ngOnInit() {
  this.getResponse();
  }

  getResponse(): void {
    let endpoint = 'users';
    this.ajax.get(endpoint).subscribe(
      res => {
        this.response = res;
        console.log(this.response);
      }
      ,
      err => {

      }

    );
  }

    deleteUser(url: string) {
      console.log("in delete in angular");
    this.ajax.delete(url).subscribe(
      res => {
        if(res == "success")
        {
        this.getResponse();
        this.alerts.setMessage('Successfully Deleted User', 'success');
        }
      },
    )
}


inserUser()
{
      this.router.navigate(['/addUser/'])

}

}
