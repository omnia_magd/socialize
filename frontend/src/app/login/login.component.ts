import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import  { AjaxService } from '../ajax.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  dataForm: object;
  constructor( private api : AjaxService,private router: Router) 
  { 
    this.dataForm = {};
    if(localStorage.getItem("user_id") == null)
    {
    window.localStorage.setItem("user_id", " ");
    }
  }

  ngOnInit() {

  }

login(form: NgForm): void {
  if(form.valid){
  console.log(this.dataForm);
    let endpoint = 'users/login';
    console.log("in login");
      this.api.post(endpoint, this.dataForm).subscribe(
        res => {
          if (res.result == "success") {
           console.log("ok yes");
           window.localStorage.setItem("user_id", res.user_id);
           window.localStorage.setItem("user_name", res.user_name);
           window.localStorage.setItem("is_admin", res.is_admin);
           location.reload();
           this.router.navigate(['/']);

          }
          
          else{
           console.log("can't");
          }
        },
        
      )
    }
   
}

}
