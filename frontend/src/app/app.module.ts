import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import  { AjaxService } from '../app/ajax.service';
import { FormsModule } from '@angular/forms';  
import { AlertsModule } from 'angular-alert-module';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { NotesComponent } from './notes/notes.component';
import { CreateNoteComponent } from './create-note/create-note.component';
import { EditNoteComponent } from './edit-note/edit-note.component';



const Routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: AppComponent },
  { path: 'users', component: UsersComponent },
  { path: 'addUser', component: CreateUserComponent },
  { path: 'editUser/:id', component: EditUserComponent },
  { path: 'notes', component: NotesComponent },
  { path: 'addNote', component: CreateNoteComponent },
  { path: 'editNote/:id', component: EditNoteComponent },
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsersComponent,
    CreateUserComponent,
    EditUserComponent,
    NotesComponent,
    CreateNoteComponent,
    EditNoteComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(Routes),
    FormsModule,
    AlertsModule.forRoot()
  ],
  providers: [AjaxService],
  bootstrap: [AppComponent]
})
export class AppModule { }
