import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  userId: any;
  log: any;
  is_admin: any;

  constructor(
		private router : Router
	){
     this.userId = window.localStorage.getItem("user_id");
     this.is_admin = window.localStorage.getItem("is_admin");
  }

    ngOnInit() 
    {

      if (this.userId == " ") 
      {
         this.log = false;
      }

     else 
      {
        this.log = true;
      }

      console.log(this.log);
  }

  getResponse () :void {
    this.router.navigate(['/users/'])
  }

    getNotes() :void {
    this.router.navigate(['/notes/'])
  }

  inserUser()
{
      this.router.navigate(['/addUser/'])
}

  logout()
{
  console.log("noooooooooooooo");
  localStorage.removeItem('user_id');
  localStorage.removeItem('user_name');
  localStorage.removeItem('is_admin');
  location.reload();
  this.router.navigate(['/']);
}


}
