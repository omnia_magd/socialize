import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http'; import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

const path: string = "http://127.0.0.1:8080/";
const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });


@Injectable()
export class AjaxService {
  data: object
  constructor(private http: HttpClient) { }


   toHttpParams(obj: Object): HttpParams {
    let params = new HttpParams();
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const val = obj[key];
        if (val !== null && val !== undefined) {
          params = params.append(key, val.toString());
        }
      }
    }
    return params;
  }


  post(endpoint: string, Data: object): Observable<any> {
    var params = this.toHttpParams(Data)
    return this.http.post(path + endpoint, params.toString(),
      { headers }
    )


}

  get(endpoint: string): Observable<any> {
    console.log(path + endpoint);
    return this.http.get(path + endpoint)
  }

   delete(endpoint: string): Observable<any> {
    return this.http.delete(path + endpoint, { headers }
    )
  } 


   put(endpoint: string, Data: object): Observable<any> {
    var params = this.toHttpParams(Data)
    return this.http.put(path + endpoint, params.toString(),
      { headers }
    )
  }
}