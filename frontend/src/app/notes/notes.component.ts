import { Component, OnInit } from '@angular/core';
import  { AjaxService } from '../ajax.service';
import { Router } from '@angular/router';
import { AlertsService } from 'angular-alert-module';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {

  notes: Array<object>;
  response: Array<object>;
  user_id: any

  constructor(private ajax : AjaxService,private router : Router,private alerts: AlertsService) 
      {
      		this.notes = [];
      }

  ngOnInit() {

   if(localStorage.getItem("user_id") != null)
    {
  		 this.user_id = window.localStorage.getItem("user_id");
    }

  console.log(this.user_id);
  console.log("in get notes ");

    let endpoint = 'notes/'+this.user_id;
    this.ajax.get(endpoint).subscribe(
      res => {
        this.response = res;
        console.log(this.response);
      }
      ,
      err => {

      }

    );

  }

  deleteNote(url: string) {

    this.ajax.delete(url).subscribe(
      res => {
        if(res == "success")
        {

            this.ngOnInit();
            console.log("deleted");
            this.alerts.setMessage('Successfully Deleted Your Note', 'success'); 
        }
      },
    )
}

  shareNote(url: string) {
      console.log("in share in angular");
    this.ajax.get(url).subscribe(
      res => {
        if(res == "success")
        {
          this.ngOnInit();
          console.log("shared ok");
          this.alerts.setMessage('Successfully Shared Your Note', 'success'); 
            
        }
      },
    )
}


  insertNote()
{
      this.router.navigate(['/addNote/'])

}
}
