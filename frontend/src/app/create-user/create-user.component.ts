import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import  { AjaxService } from '../ajax.service';
import { Router } from '@angular/router';
import { AlertsService } from 'angular-alert-module';


@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  
 dataForm: object;

  constructor(private api : AjaxService,private router: Router,private alerts: AlertsService) { 
    this.dataForm = {};
  }

  ngOnInit() {
  }

  create(form: NgForm): void {
    if(form.valid)
    {
  		console.log(this.dataForm);
  	    let endpoint = 'users/addUser';
    
      this.api.post(endpoint, this.dataForm).subscribe(
        res => {

          if (res== "success") {
            this.router.navigate(['/users']);
             console.log("ok yes");
            this.alerts.setMessage('Successfully Created User', 'success');
           
          }
          
          else{
            this.alerts.setMessage('Can not create user!', 'error');
            console.log("can't");
           }
           
        });
    }
}



}
