import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import  { AjaxService } from '../ajax.service';
import { ActivatedRoute,Router } from '@angular/router';
import { AlertsService } from 'angular-alert-module';


@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.css']
})
export class EditNoteComponent implements OnInit {

  id: any
  note:object
  dataForm: object;

  constructor(private api : AjaxService,private router: Router,private route: ActivatedRoute,private alerts: AlertsService) 
  {
     this.note = {};
    this.dataForm = {};
  }

  ngOnInit() {
       console.log("in go");
  		this.route.params.subscribe(params => {
       	this.id = params['id'];
       	console.log(this.id);
       	let endpoint = 'notes/noteData/'+this.id;
        this.api.get(endpoint).subscribe(
      res => {
          if(res.result == "success")
          {
            console.log("success");
            console.log(res.note_id);
            this.dataForm = {id:res.note_id ,note_text: res.note_text};
          }
      }

    );
       });
  }

update(form: NgForm): void {
if (form.valid) {
      console.log(this.dataForm);
        let endpoint = 'notes/editNote';
    
      this.api.put(endpoint, this.dataForm).subscribe(
        res => {
          if (res== "success") {
           this.router.navigate(['/notes/']);
           this.alerts.setMessage('Successfully Updated Your Note', 'success'); 
          }
          
          else{
            this.alerts.setMessage('Can not create note', 'error');
          
          }
        });
        }
}


}
