import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import  { AjaxService } from '../ajax.service';
import { ActivatedRoute,Router } from '@angular/router';
import { AlertsService } from 'angular-alert-module';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  id: any
  user:object
  dataForm: object;
  

  constructor(private api : AjaxService,private router: Router,private route: ActivatedRoute,private alerts: AlertsService) { 
  	this.user = {};
    this.dataForm = {};
  }

  ngOnInit() {

        this.route.params.subscribe(params => {
       	this.id = params['id'];
       	console.log(this.id);

       	let endpoint = 'users/'+this.id;
        this.api.get(endpoint).subscribe(
      res => {

          if(res.result == "success")
          {
            console.log(res.user_name);
            this.dataForm = {name:res.user_name ,email: res.user_email ,phone: res.user_phone ,id: res.user_id};
          }
      }

    );
       });
  }


  update(form: NgForm): void {
  if(form.valid)
  {
      console.log(this.dataForm);
        let endpoint = 'users/editUser';
    
      this.api.put(endpoint, this.dataForm).subscribe(
        res => {
          if (res== "success") {

            this.router.navigate(['/users/']);
            this.alerts.setMessage('Successfully Updated User data', 'success');
            console.log("ok yes");
           
          }
          
          else{
            this.alerts.setMessage('Can not Update User data', 'error');
            console.log("can't");
          }

        });
    }
}


}
