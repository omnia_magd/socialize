import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import  { AjaxService } from '../ajax.service';
import { Router } from '@angular/router';
import { AlertsService } from 'angular-alert-module';


@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.css']
})
export class CreateNoteComponent implements OnInit {
  
   dataForm: object;
   userId: any;

 constructor(private api : AjaxService,private router: Router,private alerts: AlertsService) { 
    this.dataForm = {};
    
    if(localStorage.getItem("user_id") != null)
    {
       this.userId =  window.localStorage.getItem("user_id");
    }
  }

  ngOnInit() {
  }

  create(form: NgForm): void {
 if (form.valid) {
 
       let endpoint = 'notes/addNote/'+this.userId;
  	   this.api.post(endpoint, this.dataForm).subscribe(

        res => {
          if (res== "success") {
                this.router.navigate(['/notes/']);
                this.alerts.setMessage('Successfully Created Your Note', 'success');         
          }
          
          else{
            this.alerts.setMessage('Can not Create note', 'error');
                  
          }
        });
     }
}

}
