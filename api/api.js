
const express = require('express'); //require express framework
const app = express(); 
const bodyParser = require('body-parser'); //require body-parser
var session = require('express-session');


 app.use(function (req, res, next) {
res.setHeader('Access-Control-Allow-Origin', '*');
res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
res.setHeader('Access-Control-Allow-Credentials', true);
next();
}); //content header 

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
 



// default route
app.get('/', function (req, res) {
    return res.send({ error: true, message: 'hello' })

});

//use users  apis
const routes = require('./app/routes/users');
app.use('/users', routes);

//use notes  apis
const routes_notes = require('./app/routes/notes');
app.use('/notes', routes_notes);


// all other requests redirect to 404
app.all("*", function (req, res) {
    return res.status(404).send('page not found')
});
 
// port set to 8080 because incoming http requests 
app.listen(8080, function () {
    console.log('Node app is running on port 8080');
});
 
// allows "grunt dev" to create a development server with livereload
module.exports = app;
