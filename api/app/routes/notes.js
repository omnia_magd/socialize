

const routes = require('express').Router();

var mc = require('../models/db');


// Retrieve allnotes with user
routes.get('/:id', function (req, res) {
 
    let userId = req.params.id;
    console.log("innnnnnn");
    console.log(userId);

    // mc.query('SELECT * FROM notes , users where notes.`note_creator`= ? ', userId, function (error, results, fields) {
    //     if (error) throw error;
    //     return res.json(results);

    // });

    let sql = 'SELECT * FROM notes , users where users.user_id = notes.note_creator and (notes.`note_creator`= ? or notes.is_shared = 1)';
        mc.query(sql, [userId], (err, result) => {
        if (err) throw err;
        return res.json(result);

    });
 
});


//  Delete note
routes.delete('/deleteNote/:id', function (req, res) {
  console.log("in delete in node");
     let noteId = req.params.id;
   console.log(noteId);
    mc.query('DELETE FROM notes WHERE note_id = ?', [noteId], function (error, results, fields) {
        if (error) throw error;
        return res.json("success");
    });
});

// Add a new note  
routes.post('/addNote/:id', function (req, res) {
    
    console.log("in add note");

    let userId = req.params.id;
    let note = req.body.note_text;
       
    console.log(userId);
    console.log(note);


     let sql = 'insert into notes (note_text,note_creator,is_shared) values (?,?,?)';
    mc.query(sql, [note, userId,0], (err, result) => {
    if (err) throw err;
    return res.json("success");

    });
});


// Retrieve Note with id 
routes.get('/noteData/:id', function (req, res) {
 
    let noteId = req.params.id;
    console.log("dkfndkfndkfn");
    console.log(noteId);
    mc.query('SELECT * FROM notes where note_id=?', noteId, function (error, results, fields) {
        if (error) throw error;

        console.log("ok");
        var obj = 

                {
                  'result':'success',
                  'note_text':`${results[0].note_text}`,
                  'note_id':`${results[0].note_id}`
                };

                return res.json(obj)
    });
 
});

//  Update note with id
routes.put('/editNote', function (req, res) {
 
    let _id = req.body.id;
    let note_text = req.body.note_text;

   let sql = 'update notes set note_text = ?  where note_id=? ';
        mc.query(sql, [note_text,_id], (err, result) => {
        if (err) throw err;
        return res.json("success");

    });

   
});

//share note 
routes.get('/shareNote/:id', function (req, res) {
  console.log("in share in node");
     let noteId = req.params.id;
   console.log(noteId);
    let sql = 'update notes set is_shared = ?  where note_id=? ';
        mc.query(sql, [1,noteId], (err, result) => {
        if (err) throw err;
        return res.json("success");

    });
});

module.exports = routes