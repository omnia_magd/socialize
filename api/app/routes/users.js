
const routes = require('express').Router();

var mc = require('../models/db');

// Retrieve all users 
routes.get('/', function (req, res) {
    console.log("retreive all users");
    mc.query('SELECT * FROM users', function (error, results, fields) {
        if (error) throw error;
        return res.json(results);
    });
});
 
routes.post('/login',function(req,res){
    console.log("isa");
    
    let userEmail = req.body.email;
    let userPass = req.body.pass;
    console.log(userEmail);
    console.log(userPass);
    let sql = 'SELECT * FROM users WHERE user_email = ? AND user_password = ?';

		mc.query(sql, [userEmail, userPass], (err, result) => {
		if (err) throw err;
		result.forEach( (result) => {

			var user_id = `${result.user_id}`;

          

			if(user_id != null)
			{
				var obj = 

				{
				  'result':'success',
				  'user_id':`${result.user_id}`,
				  'user_name':`${result.user_name}`,
          'is_admin':`${result.is_admin}`
			    };

				return res.json(obj)
			}
				
		    if(`${result.user_id}` == null)
		    {
                return res.json("not success")
		    }
  			
		});
        

	});
  
});


//  Delete user
routes.delete('/deleteUser/:id', function (req, res) {
  console.log("in delete in node");
     let userId = req.params.id;
   console.log(userId);
    mc.query('DELETE FROM users WHERE user_id = ?', [userId], function (error, results, fields) {
        if (error) throw error;
        return res.json("success");
    });
});


// Add a new user  
routes.post('/addUser', function (req, res) {
 
    let name = req.body.name;
    let email = req.body.email;
    let pass = req.body.pass;
    let phone = req.body.phone;
    
    console.log(name);
    console.log(email);
    console.log(pass);
    console.log(phone);

     let sql = 'insert into users (user_name,user_password , user_email ,user_phone ,is_admin) values (?,?,?,?,?)';
		mc.query(sql, [name, pass,email,phone,0], (err, result) => {
		if (err) throw err;
		return res.json("success");

    });
});


// Retrieve user with id 
routes.get('/:id', function (req, res) {
 
    let userId = req.params.id;
    console.log("innnnnnn");
    console.log(userId);
    mc.query('SELECT * FROM users where user_id=?', userId, function (error, results, fields) {
        if (error) throw error;
        var obj = 

                {
                  'result':'success',
                  'user_id':`${results[0].user_id}`,
                  'user_name':`${results[0].user_name}`,
                  'user_email':`${results[0].user_email}`,
                  'user_phone':`${results[0].user_phone}`
                };

                return res.json(obj)
    });
 
});

//  Update user with id
routes.put('/editUser', function (req, res) {
 
    let _id = req.body.id;
    let name = req.body.name;
    let pass = req.body.pass;
    let phone = req.body.phone;
    let email = req.body.email;

   let sql = 'update users set user_name = ? ,user_password =? , user_email = ? ,user_phone = ? where user_id=? ';
        mc.query(sql, [name, pass,email,phone,_id], (err, result) => {
        if (err) throw err;
        return res.json("success");

    });

   
});


module.exports = routes;


